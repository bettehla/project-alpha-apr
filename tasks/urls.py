from django.urls import path
from tasks.views import list_tasks, create_task, show_my_tasks

urlpatterns = [
    path("", list_tasks, name="list_tasks"),
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
